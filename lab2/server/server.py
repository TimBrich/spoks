import socket
import datetime
import threading
from time import sleep
from struct import pack, unpack

lock = threading.Lock()

def download(sock, addr, fname, err):
    #try open file
    try:
        fl = open(fname, 'rb')
        print(fl)
    except:
        print('error file name')
        if not send(sock, addr, 'error file name'):
            print('error send error file name')
        return err

    #send size
    size = len(fl.read())
    if size == 0:
        print('error file size = 0')
        if not send(sock, addr, 'error file size = 0'):
            print('conn lose')
        fl.close()
        return err

    fl.seek(0)
    if not send(sock, addr, str(size)):
        print('error send size')
        return err

    #send pos
    if err['flag'] == True and err['file'] == fname and err['flag_last_addr'] == True and err['type'] == 'download':
        pos = int(err['pos'])
        if not send(sock, addr, str('pos ' + str(pos))):
            print('error send pos')
            return err
        err['flag'] = False

    else:
        pos = 0
        if not send(sock, addr, 'pos 0'):
            print('error send pos = 0')
            return err

    fl.seek(pos)
    er = slidingWindowDownload(sock, addr, fl, size, pos, 30)
    if er:
        print('download interrupt')
        err['file'] = fname
        err['pos'] = int(er)
        err['type'] = 'download'
        err['flag'] = True
        return err

    print('end upload')
    return err

def slidingWindowDownload(sock, addr, fl, size, _pos, window_size):
    timeo = sock.gettimeout()
    sock.settimeout(0.1)

    global stop_recv_thread
    global lastAccept
    global currentSend
    global loseConn

    stop_recv_thread = False
    currentSend = 0
    lastAccept = -1

    loseConn = False

    pos = _pos

    def acpListen():
        global lastAccept
        global currentSend
        global stop_recv_thread
        global loseConn

        bag = 0

        while not stop_recv_thread:
            try:
                msg = sock.recvfrom(1024)[0]

            except socket.timeout:
                bag += 1
                lock.acquire()
                currentSend = lastAccept + 1
                lock.release()
                if bag >= 5:
                    loseConn = True
                    return
                continue

            except ConnectionResetError or (BrokenPipeError, IOError):
                loseConn = True
                return

            lock.acquire()
            lastAccept = unpack('Q', msg)[0]
            print(lastAccept)
            lock.release()

    recv_thread = threading.Thread(target=acpListen, args=())
    recv_thread.start()

    while lastAccept != ((size - _pos) // (1024 - 8)):
        lock.acquire()
        current = currentSend
        acc = lastAccept
        lock.release()

        if loseConn:
            lock.acquire()
            stop_recv_thread = True
            recv_thread.join()
            sock.settimeout(timeo)
            accept = lastAccept
            lock.release()
            return accept * (1024 - 8)

        if current - acc > window_size:
            sleep(0.001)
            continue

        if _pos + current * (1024 - 8) >= size:
            sleep(0.001)
            continue

        fl.seek(_pos + current * (1024 - 8))
        msg = fl.read(1024 - 8)
        msg = pack('Q', current) + msg
        sock.sendto(msg, addr)

        lock.acquire()
        currentSend += 1
        lock.release()

    stop_recv_thread = True
    recv_thread.join()
    sock.settimeout(timeo)

def upload(sock, addr, fname, err):
    if err['flag'] == True and err['file'] == fname and err['flag_last_addr'] == True and err['type'] == 'upload':
        pos = int(err['pos'])
        send(sock, addr, str('pos ' + str(err['pos'])))
        err['flag'] = False
    else:
        pos = 0
        send(sock, addr, 'pos 0')

    size = int(recv(sock, addr))
    if size == 0:
        send(sock, addr, 'error')
        fl.close()
        return err

    if pos == 0:
        fl = open(fname, 'wb')
    else:
        buf = fl.read(pos)
        fl.close()
        fl = open(fname, 'wb')
        fl.write(buf)
        fl.seek(pos)
    print('pos = ' , pos)
    fl_size = pos
    buf = 0
    for i in range(int(size / 1024) + 1):
        if buf == 0:
            msg = recv(sock, addr, byte_flag=True)
            if msg == '' or 'close'.encode('utf-8') in msg:
                print('upload interrupt')
                err['file'] = fname
                err['pos'] = int(fl_size)
                err['type'] = 'upload'
                err['flag'] = True
                return err

            fl.write(msg)
            fl_size += len(msg)
            if size == fl_size:
                buf = 1

    print('end download')
    return err

#addr = ip,port
def recv(sock, addr, max_count = 10, byte_flag = False):
    count = 0
    msg = ''
    while True:
        try:
            data, re_addr = sock.recvfrom(1024)
            if re_addr[0] == addr[0]:
                if byte_flag:
                    msg = data
                else:
                    msg = data.decode('utf-8')
                break

            else:
                count += 1
                if count >= max_count:
                    return ''

        except Exception as e:
            count += 1
            if count >= max_count:
                return ''

    if byte_flag:
        sock.sendto(msg, addr)
    else:
        sock.sendto(msg.encode('utf-8'), addr)
    return msg

#addr = ip,port
#3 try send
def send(sock, addr, msg, max_count = 3, byte_flag = False):
    for i in range(3):
        if byte_flag:
            sock.sendto(msg, addr)
        else:
            sock.sendto(msg.encode('utf-8'), addr)
        count = 0
        while True:
            try:
                data, re_addr = sock.recvfrom(1024)
                if re_addr[0] == addr[0]:
                    if byte_flag:
                        ans = data
                    else:
                        ans = data.decode('utf-8')

                    if msg == ans:
                        return True
                    else:
                        count += 1
                        if count >= max_count:
                            break

                else:
                    sock.sendto('error'.encode('utf-8'), re_addr)

            except:
                print('not ack send')
                count += 1
                if count >= max_count:
                    break
    return False

def conn(sock):
    count = 0
    msg = None
    addr = None
    while True:
        try:
            msg, addr = sock.recvfrom(1024)
            break
        except:
            count += 1
            print('not have client', str(count*3) + 'sek')
            continue
    try:
        if msg.decode("utf-8") == 'SYN':
            sock.sendto('SYN+ACK'.encode("utf-8"), addr)
            msg = sock.recvfrom(1024)[0]
            if msg.decode("utf-8") == "ACK":
                sock.sendto('START'.encode("utf-8"), addr)
                return addr

    except Exception as e:
        print(e)

    return '',0

def startServer(ip, port, timeout):
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.bind((ip, port))
    s.settimeout(timeout)

    addr = ('','')

    errors = {'flag':False,
              'file':'',
              'pos': 0,
              'type':'',
              'last addr':'',
              'flag_last_addr': False}

    while True:
        errors['last addr'] = addr[0]
        addr = conn(s)
        if addr[0] == '':
            print("unsuccessful try connect")
            continue
        errors['flag_last_addr'] = errors['last addr'] == addr[0]

        while True:
            msg = recv(s, addr, 60 / 6) #if client not send more then 3 / 6 min
            if msg == '':
                print('conn lose')
                break
            print('msg = ' + msg)

            if 'etho' in msg:
                if not send(s, addr, msg[5:]):
                    print('conn lose')
                    break
                continue

            if 'datetime' in msg:
                if not send(s, addr, str(datetime.datetime.now())):
                    print('conn lose')
                    break
                continue

            if 'close' in msg:
                break

            if 'exit' in msg:
                s.close()
                return

            if 'download' in msg:
                print(errors)
                errors = download(s, addr, msg[9:], errors)
                continue

            #1-com, 2-size, 3-accept, 4-download
            if 'upload' in msg:
                print(errors)
                errors = upload(s, addr, msg[7:], errors)
                continue

            print('uncknown command')

print('input ip:')
ip = input()

startServer(ip, 8888, 3)
