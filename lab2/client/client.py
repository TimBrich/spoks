import socket
from tqdm import trange, tqdm
import threading
from time import sleep
from struct import pack, unpack
import datetime

lock = threading.Lock()

def download(sock, addr, fname):
    #recv file size or error msg
    msg = recv(sock, addr)
    if msg == '':
        print('con lose')
        return 'exit'
    print('size = ', msg)
    size = int(msg)

    #recv start pos
    msg = recv(sock, addr)
    if 'pos' in msg:
        pos = int(msg[4:])
        print('pos = ', pos)
    else:
        print(msg)
        return

    #open file and download
    if pos != 0:
        fl = open(fname, 'rb')
        buf = fl.read(pos)
        fl.close()
        fl = open(fname, 'wb')
        fl.write(buf)
    else:
        fl = open(fname, 'wb')

    fl.seek(pos)
    slidingWindowDownload(sock, addr, fl, size, pos)

    print('download end')
    return

def upload(sock, addr, fname):
    try:
        fl = open(fname, 'rb')
        size = len(fl.read())
        if size == 0:
            print('error size file')
            fl.close()
            return

    except:
        print('error file name')
        return

    send(sock, addr, str('upload ' + fname))

    buf = recv(sock, addr)
    if not buf:
        print('con lose')
        return 'exit'

    if 'pos' not in buf:
        print('upload error')
        return 'exit'

    pos = int(buf[4:])
    fl.seek(pos)
    print('pos = ' , pos)

    send(sock, addr, str(size))

    s_size = pos
    buf = 0
    for i in trange(int(size / 1024) + 1, desc=('upload ' + fname), unit=' KB'):
        if buf == 0:
            txt = fl.read(1024)
            if not send(sock, addr, txt, byte_flag=True):
                return 'exit'

            s_size += len(txt)
            if s_size == size:
                buf = 1

    print('end upload')
    return

def slidingWindowDownload(sock, addr, fl, size, _pos):
    timeo = sock.gettimeout()
    sock.settimeout(10)

    waitingIndex = 0
    recv_buf = {}
    pos = _pos

    with tqdm(total=size // 1024 + 1, desc=('download ' + fl.name), unit=' KB') as pbar:
        pbar.update(pos / 1024)
        while pos != size:
            try:
                msg = sock.recvfrom(1024)[0]
            except socket.timeout:
                return 'exit'

            index = unpack('Q',msg[0:8])[0]
            recv_buf[index] = msg[8:]

            if waitingIndex in recv_buf:
                while waitingIndex in recv_buf:
                    fl.write(recv_buf[waitingIndex])
                    pos += len(recv_buf[waitingIndex])
                    pbar.update(len(recv_buf[waitingIndex]) / 1024)
                    del recv_buf[waitingIndex]
                    waitingIndex += 1
                sock.sendto(pack('Q', waitingIndex - 1), addr)

    sock.settimeout(timeo)

def recv(sock, addr, max_count = 10, byte_flag = False):
    count = 0
    msg = ''
    while True:
        try:
            data, re_addr = sock.recvfrom(1024)
            if re_addr[0] == addr[0]:
                if byte_flag:
                    msg = data
                else:
                    msg = data.decode('utf-8')
                break

            else:
                count += 1
                if count >= max_count:
                    return ''

        except Exception as e:
            print(e)
            count += 1
            if count >= max_count:
                return ''

    if byte_flag:
        sock.sendto(msg, addr)
    else:
        sock.sendto(msg.encode('utf-8'), addr)
    return msg

#addr = ip,port
#3 try send
def send(sock, addr, msg, max_count = 3, byte_flag = False):
    for i in range(3):
        if byte_flag:
            sock.sendto(msg, addr)
        else:
            sock.sendto(msg.encode('utf-8'), addr)
        count = 0
        while True:
            try:
                data, re_addr = sock.recvfrom(1024)
                if re_addr[0] == addr[0]:
                    if byte_flag:
                        ans = data
                    else:
                        ans = data.decode('utf-8')
                    if msg == ans:
                        return True
                    else:
                        count += 1
                        if count >= max_count:
                            break
                else:
                    sock.sendto('error'.encode('utf-8'), re_addr)

            except:
                count += 1
                if count >= max_count:
                    break
    return False

def clientCommand(sock, addr, txt):
    try:
        if 'close' in txt or 'exit' in txt:
            send(sock, addr, txt)
            sock.close()
            return 'exit'
        if 'etho' in txt or 'datetime' in txt:
            send(sock, addr, txt)
            ans = recv(sock, addr)
            if ans != '':
                print(ans)
            else:
                print('conn lose')
                return 'exit'
            return ''
        if 'download' in txt:
            send(sock, addr, str('download ' + txt[9:]))
            return download(sock, addr, txt[9:])
            #send com
        #1-com, 2-size/error, 3-upload
        if 'upload' in txt:
            return upload(sock, addr, txt[7:])

        print('uncknown command')
    except KeyboardInterrupt:
        return 'exit'

def conn(sock, addr):
    try:
        sock.sendto('SYN'.encode('utf-8'), addr)
        if sock.recvfrom(1024)[0].decode('utf-8') == 'SYN+ACK':
            sock.sendto('ACK'.encode('utf-8'), addr)
            if sock.recvfrom(1024)[0].decode('utf-8') == 'START':
                return True
            else:
                return False
                print('error step2')
        else:
            print('error step1')
            return False
    except Exception as e:
        print(e)
        return False

def startClient(ip, port, ip2, port2, timeout):
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.bind((ip, port))
    s.settimeout(timeout)

    addr = (ip2, port2)

    if not conn(s, addr):
        print('error connect')
        return

    while True:
        inp = input()
        ans = clientCommand(s, addr, inp)
        if ans == 'exit':
            break

    print('close program')
    return


print('input computer ip:')
ip = input()

print('input server ip:')
ip2 = input()

startClient(ip, 8887, ip2, 8888, 3)
