import socket
from tqdm import trange

def download(fname):
    #recv file size or error msg
    msg = s.recv(1024).decode('utf-8')
    if not msg:
        print('con lose')
        return 'exit'

    msg = msg.split(' ')
    print(msg)
    size = int(msg[0])
    pos = int(msg[1])

    #open file and download
    if pos != 0:
        fl = open(fname, 'rb')
        buf = fl.read(pos)
        fl.close()
        fl = open(fname, 'wb')
        fl.write(buf)
    else:
        fl = open(fname, 'wb')

    s.send(b'start')
    fl_size = pos
    buf = 0
    for i in trange(int(size / 1024) + 1, desc=('download ' + fname), unit=' KB'):
        if buf == 0:
            try:
                msg = s.recv(1024)
                fl.write(msg)
                fl_size += len(msg)
                #print(str(size) + '/' + str(fl_size))
                s.send(str(len(msg)).encode('utf-8'))
                if size == fl_size:
                    buf = 1
            except ConnectionResetError or BrokenPipeError:
                print('conn lose')
                s.send('close conn'.encode('utf-8'))
                s.close()
                return 'exit'

    print('download end')
    return

def upload(fname):
    try:
        fl = open(fname, 'rb')
        size = len(fl.read())
        if size == 0:
            print('error size file')
            fl.close()
            return

    except:
        print('error file name')
        return

    s.send(str('upload ' + fname).encode('utf-8'))
    buf = s.recv(1024).decode('utf-8[')
    if not buf:
        print('con lose')
        return 'exit'

    if 'pos' not in buf:
        print('upload error')
        return 'exit'

    pos = int(buf[4:])
    fl.seek(pos)
    print('pos = ' , pos)

    s.send(str(size).encode('utf-8'))

    buf = s.recv(1024).decode('utf-8')
    try:
        if buf != str(size):
            print('error upload')
            return
    except:
        print(buf)
        return

    print('end init')
    s_size = pos
    buf = 0
    for i in trange(int(size / 1024) + 1, desc=('upload ' + fname), unit=' KB'):
        if buf == 0:
            txt = fl.read(1024)
            s.send(txt)
            try:
                c_size = int(s.recv(1024).decode('utf-8'))
            except:
                print('error recv')
                s.send('close conn'.encode('utf-8'))
                s.close()
                return 'exit'

            if len(txt) != c_size:
                print('error size')
                s.send('error send file')
                return

            s_size += c_size
            if s_size == size:
                buf = 1

    print('end upload')
    return

def send(txt):
    try:
        if 'close' in txt or 'exit' in txt:
            s.send(txt.encode('utf-8'))
            s.close()
            return 'exit'
        if 'etho' in txt or 'datetime' in txt:
            s.send(txt.encode('utf-8'))
            print(s.recv(1024).decode('utf-8'))
            return
        if 'download' in txt:
            s.send(str('download ' + txt[9:]).encode('utf-8'))
            return download(txt[9:])
            #send com
        #1-com, 2-size/error, 3-upload
        if 'upload' in txt:
            return upload(txt[7:])

        print('uncknown command')
    except KeyboardInterrupt:
        return 'exit'

print('input ip:')
ip = input()
port = 65432

try:
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((ip, port))

except ConnectionResetError or ConnectionAbortedError or BrokenPipeError:
    print('disconnect')
    exit()

s.settimeout(10)

while True:
    inp = input()
    try:
        if send(inp) == 'exit': break
    except ConnectionResetError or BrokenPipeError:
        print('conn lose')
        break
