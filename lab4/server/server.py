import selectors
import socket
import types
from time import sleep
from mySocket import mySocket
from multiprocessing import Process, Manager

def connect(sock, addr, global_sock):
    print('accept ', addr)
    conn = None
    if addr[0] not in global_sock:
        conn = mySocket(sock)
    else:
        conn = mySocket(sock, global_sock[addr[0]])

    ans = conn.doing()
    if ans == 'disconnect' or 'close' in ans or 'exit' in ans:
        print('disconnect', addr)
        conn.disconnect()
        global_sock[addr[0]] = conn.errors

if __name__ == '__main__':
    manager = Manager()
    global_sock = manager.dict()
    print('input ip')
    host = input()
    port = 65432        # The port used by the server

    lsock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    lsock.bind((host, port))
    lsock.listen()
    print('listening on', (host, port))

    while True:
        conn, addr = lsock.accept()
        proc = Process(target=connect, args=(conn, addr, global_sock))
        proc.start()
        print(global_sock)
