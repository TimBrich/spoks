import selectors
import socket
import types
from time import sleep
from mySocket import mySocket

host = input()
port = 65432

sel = selectors.DefaultSelector()
global global_sock
global_sock = {}

def accept_wrapper(sock):
    print(global_sock)
    conn, addr = sock.accept()
    print('accepted connection from', addr)
    conn.setblocking(False)
    data = types.SimpleNamespace(addr=addr, inb=b'', outb=b'')
    events = selectors.EVENT_READ | selectors.EVENT_WRITE
    sel.register(conn, events, data=data)

    if len(global_sock) != 0 and addr[0] in global_sock:
        global_sock[addr[0]].reconnect(conn)
    else:
        global_sock[addr[0]] = mySocket(conn)

def service_connection(key, mask):
    sock = key.fileobj
    data = key.data
    if not global_sock[sock.getpeername()[0]].resultDo():
        sock.setblocking(True)
        if global_sock[sock.getpeername()[0]].doing('') == 'disconnect':
            print('closing connectifon to', data.addr)
            sel.unregister(sock)
            global_sock[sock.getpeername()[0]].disconnect()
        else:
            sock.setblocking(False)
        return

    if mask & selectors.EVENT_READ:
        try:
            recv_data = sock.recv(1024)
        except:
            recv_data = b''

        if recv_data:
            data.outb += recv_data
            print(recv_data)


        else:
            print('closing connection to', data.addr)
            sel.unregister(sock)
            global_sock[sock.getpeername()[0]].disconnect()

    if mask & selectors.EVENT_WRITE:
        if data.outb:
            sock.setblocking(True)
            global_sock[sock.getpeername()[0]].doing(recv_data.decode('utf-8'))
            sock.setblocking(False)
            sent = len(data.outb)
            data.outb = data.outb[sent:]

def Server():
    lsock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    lsock.bind((host, port))
    lsock.listen()
    print('listening on', (host, port))
    lsock.setblocking(False)
    sel.register(lsock, selectors.EVENT_READ, data=None)

    while True:
        events = sel.select(timeout=None)
        for key, mask in events:
            if key.data is None:
                accept_wrapper(key.fileobj)
            else:
                service_connection(key, mask)

while True:
    try:
        Server()
    except OSError as e:
        print(e)
        continue
