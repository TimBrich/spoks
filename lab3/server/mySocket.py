import socket
import datetime
from time import sleep

class myDownload:
    def __init__(self, conn, fname, pos = 0, steps = 10):
        self.fname = fname
        self.pos = pos
        self.error = ''
        self.conn = conn
        self.steps = steps

        try:
            self.fl = open(fname, 'rb')
            self.size = len(self.fl.read())
            self.fl.seek(pos)
            if self.size == 0:
                self.error = 'Error size file'
                return

        except FileNotFoundError:
            self.error = 'Error file name'
            return

        return

    def init(self):
        msg = str(self.size) + ' ' + str(self.pos)
        self.conn.send(msg.encode('utf-8'))
        ans = self.conn.recv(1024).decode('utf-8')
        if 'Error' in ans:
            self.error = msg
            print(msg)
            return

        if 'start' in ans:
            return
        else:
            self.error = 'Error uncknown'
            return

    def do(self):
        fl = self.fl
        conn = self.conn

        for i in range(self.steps):
            try:
                txt = fl.read(1024)
                conn.send(txt)
                self.pos += int(conn.recv(1024).decode('utf-8'))

            except:
                print('error send')
                return 'error'

            if self.pos == self.size:
                return True

        return False

class myUpload:
    def __init__(self, conn, fname, pos = 0, steps = 10):
        self.fname = fname
        self.pos = pos
        self.error = None
        self.conn = conn
        self.steps = steps

        try:
            fl = open(self.fname, 'rb')
            txt = fl.read()
            fl.close()
            self.fl = open(self.fname, 'wb')
            self.fl.write(txt)
            self.fl.seek(pos)

        except FileNotFoundError:
            self.fl = open(self.fname, 'wb')

        self.size = 0

        return

    def init(self):
        print('start init')
        self.conn.send(str('pos ' + str(self.pos)).encode('utf-8'))
        try:
            msg = self.conn.recv(1024).decode('utf-8')
            if 'Error' in msg:
                self.error = msg
                return

            self.size = int(msg)

        except:
            self.error = 'Error recv in upload'

        self.conn.send(str(self.size).encode('utf-8'))
        print('end init')

    def do(self):
        fl = self.fl
        conn = self.conn
        for i in range(self.steps):
            try:
                msg = conn.recv(1024)
                if 'close'.encode('utf-8') in msg:
                    return 'error'

                fl.write(msg)
                self.pos += len(msg)
                #print(str(size) + '/' + str(fl_size))
                self.conn.send(str(len(msg)).encode('utf-8'))

            except:
                print('conn lost')
                return 'error'

            if self.pos == self.size:
                fl.close()
                return True

        return False

class mySocket:
    def __init__(self, conn):
        self.conn = conn
        self.addr = conn.getsockname()
        self.pos = 0
        self.errors = { 'flag':False,
                        'file':'',
                        'pos': 0,
                        'type':''}
        self.result = True
        self.do = None

    def resultDo(self):
        return self.result

    def reconnect(self, conn):
        self.conn = conn
        self.pos = 0

    def disconnect(self):
        if not self.result:
            self.errors = {
                'flag':True,
                'file':self.do.fname,
                'pos':self.do.pos,
                'type':self.do.__class__
            }
            self.result = True

        self.pos = 0
        self.conn.close()

    def getAddr(self):
        return self.addr

    def getPos(self):
        return self.pos

    def doing(self, msg):
        #pos = 0
        #делаем атомарные операции эхо, время, выход
        if self.pos == 0:
            if 'etho' in msg:
                self.conn.send(msg[5:].encode('utf-8'))
                return True

            if 'datetime' in msg:
                self.conn.send(str(datetime.datetime.now()).encode('utf-8'))
                return True

            if 'close' in msg:
                self.result = 'close'
                return True

            if 'exit' in msg:
                self.conn.close()
                self.result = 'close | exit'
                return True

            if 'download' in msg:
                pos = 0
                if self.errors['type'] == myDownload and self.errors['flag']:
                    if self.errors['file'] == msg[9:]:
                        pos = self.errors['pos']
                        self.errors = { 'flag':False,
                                        'file':'',
                                        'pos': 0,
                                        'type':''}

                self.do = myDownload(self.conn, msg[9:], pos)
                self.do.init()
                self.pos = 1
                self.result = False
                return True

            if 'upload' in msg:
                pos = 0
                if self.errors['type'] == myUpload and self.errors['flag']:
                    if self.errors['file'] == msg[7:]:
                        pos = self.errors['pos']
                        self.errors = { 'flag':False,
                                        'file':'',
                                        'pos': 0,
                                        'type':''}

                self.do = myUpload(self.conn, msg[7:], pos)
                self.do.init()
                self.pos = 1
                self.result = False
                return True

        if self.pos == 1:
            ans = self.do.do()
            if ans == 'error':
                return 'disconnect'
            if ans:
                print(self.do.do, 'success end')
                self.pos = 0
                self.result = True
                return True
            else:
                self.ressult = False
                return False
        return True
