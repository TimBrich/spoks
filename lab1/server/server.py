import socket
import datetime

def download(fname, err):
    try:
        fl = open(fname, 'rb')
        print(fl)
    except:
        print('error file name')
        conn.send('error file name'.encode('utf-8'))
        return err

    size = len(fl.read())
    if size == 0:
        print('error file size = 0')
        conn.send('error file size = 0'.encode('utf-8'))
        fl.close()
        return err

    fl.seek(0)
    conn.send(str(size).encode('utf-8'))
    msg = conn.recv(1024).decode('utf-8')
    print(msg)
    if msg != str(size):
        print('error upload')
        conn.send('error download'.encode('utf-8'))
        return err

    #recv pos
    if err['flag'] == True and err['file'] == fname and err['flag_last_addr'] == True and err['type'] == 'download':
        pos = int(err['pos'])
        conn.send(str('pos ' + str(err['pos'])).encode('utf-8'))
        err['flag'] = False

    else:
        pos = 0
        conn.send(str('pos 0').encode('utf-8'))

    if conn.recv(1024).decode('utf-8') != str(pos):
        conn.send('error pos'.encode('utf-8'))
        return err

    fl.seek(pos)

    s_size = pos
    while True:
        try:
            txt = fl.read(1024)
            conn.send(txt)
            c_size = int(conn.recv(1024).decode('utf-8'))

        except:
            print('error send')
            err['file'] = fname
            err['pos'] = int(s_size)
            err['type'] = 'download'
            err['flag'] = True
            return err

        s_size += c_size
        if s_size == size:
            break
    print('end upload')
    return err

def upload(fname, err):
    print(err)
    if err['flag'] == True and err['file'] == fname and err['flag_last_addr'] == True and err['type'] == 'upload':
        pos = int(err['pos'])
        conn.send(str('pos ' + str(err['pos'])).encode('utf-8'))
        err['flag'] = False
    else:
        pos = 0
        conn.send('pos 0'.encode('utf-8'))

    try:
        size = int(conn.recv(1024).decode('utf-8'))
        if size == 0:
            conn.send('error'.encode('utf-8'))
            return err
        conn.send(str(size).encode('utf-8'))
    except:
        conn.send('error'.encode('utf-8'))
        return err

    if pos == 0:
        fl = open(fname, 'wb')
        fl.seek(pos)
    else:
        buf = fl.read(pos)
        fl.close()
        fl = open(fname, 'wb')
        fl.write(buf)
    print('pos = ' , pos)
    fl_size = pos
    buf = 0
    for i in range(int(size / 1024) + 1):
        if buf == 0:
            try:
                msg = conn.recv(1024)
                if 'close'.encode('utf-8') in msg:
                    print('conn lose')
                    err['file'] = fname
                    err['pos'] = int(fl_size)
                    err['type'] = 'upload'
                    err['flag'] = True
                    return err
                fl.write(msg)
                fl_size += len(msg)
                #print(str(size) + '/' + str(fl_size))
                conn.send(str(len(msg)).encode('utf-8'))
                if size == fl_size:
                    buf = 1
            except ConnectionResetError or ConnectionAbortedError or (BrokenPipeError, IOError):
                print('conn lose')
                err['file'] = fname
                err['pos'] = int(fl_size)
                err['type'] = 'upload'
                err['flag'] = True
                return err

    print('end download')
    return err

print('input ip:')
ip = input()

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((ip, 8888))
s.listen(1)
addr = ['','']

errors = {'flag':False,
          'file':'',
          'pos': 0,
          'type':'',
          'last addr':['',''],
          'flag_last_addr': False}

while True:
    errors['last addr'] = addr
    conn, addr = s.accept()
    errors['flag_last_addr'] = errors['last addr'][0] == addr[0]

    try:
        while True:
            msg = conn.recv(1024).decode('utf-8')
            if not msg:
                print('conn lose')
                break
            print('msg = ' + msg)
            if 'etho' in msg:
                conn.send(msg[5:].encode('utf-8'))
                continue
            if 'datetime' in msg:
                conn.send(str(datetime.datetime.now()).encode('utf-8'))
                continue
            if 'close' in msg:
                conn.close()
                break
            if 'exit' in msg:
                conn.close()
                exit()
            if 'download' in msg:
                print(errors)
                errors = download(msg[9:], errors)
                continue

            #1-com, 2-size, 3-accept, 4-download
            if 'upload' in msg:
                errors = upload(msg[7:], errors)
                continue

    except ConnectionResetError or ConnectionAbortedError or BrokenPipeError:
        print('disconnect')
